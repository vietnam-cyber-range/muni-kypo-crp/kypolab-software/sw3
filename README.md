## Software inteligentního asistenta pro hraní bezpečnostních her (VI20202022158-R3)

Software poskytuje tři funkcionality pro hraní bezpečnostních her: adaptivní hry, variantní zadání a mapu her. Nástroj pro adaptivní hry analyzuje data z herního portálu pomocí [Software pro sběr a vyhodnocení dat z bezpečnostních her](https://gitlab.ics.muni.cz/muni-kypo-crp/kypolab-software/sw2) s cílem přizpůsobit náročnost hry tak, aby hráči dostávali úkoly adekvátní obtížnosti. Nástroj pro variantní zadání umožňuje vytváření personalizovaných zadání pro účastníky lineárních her. Tyto zadání znesnadňují podvádění při hře, které je nežádoucí při použití her pro hodnocení studentů nebo v soutěžích. Mapa her umožňuje systematizaci vytvořených bezpečnostních her včetně jejich cílů učení za účelem doporučení dalších her, které prohloubí dovednosti účastníka v dané či příbuzné oblasti.

### Obsah repozitářů

Tento repozitář slouží jako rozcestník pro služby a komponenty tohoto software. Níže je seznam projektů backendové a frontendové části software. Komponenty jsou plně integrovány do uživatelského rozhraní [Software pro přípravu a realizaci bezpečnostních her](https://gitlab.ics.muni.cz/muni-kypo-crp/kypolab-software/sw1). Každá položka obsahuje podrobnější instrukce k sestavení a spuštení.

### Java Backend
- [KYPO Adaptive Training](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-adaptive-training)
- [KYPO Adaptive Smart Assistant](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-adaptive-smart-assistant)
- [KYPO Answers Storage](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-answers-storage)
- [KYPO ElasticSearch Service](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-elasticsearch-service)
- [KYPO ElasticSearch Documents](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-elasticsearch-documents)

### Angular Frontend
- [KYPO Training](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/agendas/kypo-training-agenda)
- [KYPO Adaptive Model Simulator](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/components/kypo-adaptive-model-simulator)
- [KYPO Adaptive Trainings Visualizations](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/components/kypo-adaptive-trainings-visualizations)
- [KYPO Adaptive Transition Visualization](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/components/kypo-adaptive-transition-visualization)

### Poděkování

<table>
  <tr>
    <td>![MV ČR](logo_mvcr.png "Logo MV ČR")</td>
    <td>
Tento software je výsledkem projektu Výzkum nových technologií pro zvýšení schopností odborníků na kyberbezpečnost – VI20202022158, který byl podpořen Ministerstvem vnitra ČR z Programu bezpečnostního výzkumu České republiky 2015-2022 (BV III/1-VS).
</td>
  </tr>
</table>
